import random
import os


class ProxyRotator(object):
    """This loads proxies from the proxy_list.txt file and returns a random one.

    Parameters:
    -----------
    path: str
        file name of the proxy list where the proxies are listed and delimited
        by a new line
    """

    # the path is hard coded. make this more modular
    def __init__(self, path='proxy_list.txt'):
        if path == "proxy_list.txt":
            dir_path = os.path.dirname(os.path.realpath(__file__))
            proxy_list_path = "{dir_path}/{path}".format(dir_path=dir_path,
                                                         path=path)
        else:
            proxy_list_path = path
        self.proxy_list = self.__load_proxies(path=proxy_list_path)

    def __load_proxies(self, path):
        proxies = []
        with open(path, 'r') as file:
            for line in file.readlines():
                proxies.append(line.strip())
        return proxies

    @property
    def proxies(self):
        proxy = random.choice(self.proxy_list)
        proxies_dict = {
            'http' : proxy,
            'https': proxy
        }
        return proxies_dict
